package pt.cocus.nce.rest.ws.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
@Configuration(value = DataSourceConfigProperties.BEAN_NAME)
@ConfigurationProperties(prefix = DataSourceConfigProperties.CONFIG_PROPERTIES_PREFIX)
public class DataSourceConfigProperties {

    public static final String BEAN_NAME = "config.DataSourceConfigProperties";
    
    public static final String CONFIG_PROPERTIES_PREFIX = "jdbc";
        
    private String driver;
    private String url;
    private String username;
    private String password;
    private String mapperConfigPath;
    private String mapperLocation;
    private String entityPackage;
    private String migrationScriptPath;
    private boolean validateMigrationNaming;
    private boolean validateOnMigrate;
    
    public String getDriver() {
        return driver;
    }
    public void setDriver(String driver) {
        this.driver = driver;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getMapperConfigPath() {
        return mapperConfigPath;
    }
    public void setMapperConfigPath(String mapperConfigPath) {
        this.mapperConfigPath = mapperConfigPath;
    }
    public String getMapperLocation() {
        return mapperLocation;
    }
    public void setMapperLocation(String mapperLocation) {
        this.mapperLocation = mapperLocation;
    }    
    
    public String getEntityPackage() {
        return entityPackage;
    }
    public void setEntityPackage(String entityPackage) {
        this.entityPackage = entityPackage;
    }
    public String getMigrationScriptPath() {
        return migrationScriptPath;
    }
    public void setMigrationScriptPath(String migrationScriptPath) {
        this.migrationScriptPath = migrationScriptPath;
    }
    
    public boolean isValidateMigrationNaming() {
        return validateMigrationNaming;
    }
    public void setValidateMigrationNaming(boolean validateMigrationNaming) {
        this.validateMigrationNaming = validateMigrationNaming;
    }
    public boolean isValidateOnMigrate() {
        return validateOnMigrate;
    }
    public void setValidateOnMigrate(boolean validateOnMigrate) {
        this.validateOnMigrate = validateOnMigrate;
    }

}
