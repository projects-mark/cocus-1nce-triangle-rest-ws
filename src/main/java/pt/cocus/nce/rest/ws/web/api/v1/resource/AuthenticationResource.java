package pt.cocus.nce.rest.ws.web.api.v1.resource;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.cocus.nce.rest.ws.core.exceptions.SQLException;
import pt.cocus.nce.rest.ws.core.exceptions.StandardException;
import pt.cocus.nce.rest.ws.model.dto.request.AuthenticationLogonRequest;
import pt.cocus.nce.rest.ws.model.dto.request.AuthenticationRegisterRequest;
import pt.cocus.nce.rest.ws.model.dto.response.AuthenticationResponse;
import pt.cocus.nce.rest.ws.service.abstractions.AuthenticationServiceAbstraction;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/authentication/")
public class AuthenticationResource {

    private final AuthenticationServiceAbstraction abstractAuthenticationHandler;
    
    @Autowired    
    public AuthenticationResource(@Qualifier(value = AuthenticationServiceAbstraction.BEAN_NAME) final AuthenticationServiceAbstraction abstractAuthenticationHandler) {
        this.abstractAuthenticationHandler = abstractAuthenticationHandler;
    }

    @PostMapping(value = "sign-on", consumes = "application/json", produces = "application/json")
    public ResponseEntity<AuthenticationResponse> processSignRequest(@Valid @RequestBody final AuthenticationLogonRequest logonRequest) throws StandardException, SQLException {
        final AuthenticationResponse response = abstractAuthenticationHandler.processSignOn(logonRequest);
        return new ResponseEntity<>(response, response.getStatusCode());
    }
    
    @PostMapping(value = "on-board", consumes = "application/json", produces = "application/json")
    public ResponseEntity<AuthenticationResponse> processOnboardingRequest(@Valid @RequestBody final AuthenticationRegisterRequest registerRequest) throws StandardException, SQLException {
        final AuthenticationResponse response = abstractAuthenticationHandler.processOnboarding(registerRequest);
        return new ResponseEntity<>(response, response.getStatusCode());
    }
}
