package pt.cocus.nce.rest.ws.core.utils;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.util.StopWatch;
import pt.cocus.nce.rest.ws.core.exceptions.SQLException;
import pt.cocus.nce.rest.ws.core.exceptions.StandardException;
import pt.cocus.nce.rest.ws.model.enums.SQLStatusEnum;
import pt.cocus.nce.rest.ws.model.enums.StandardStatusEnum;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
public class JdbcRequestUtil {
	
	private JdbcRequestUtil() {}
	
	private static Logger log = LoggerFactory.getLogger(JdbcRequestUtil.class);
	
	public static final String A_FUNCTION_FIELD = "aFunction";
	
    public static <S> S select(Supplier<S> aSupplier, String aPotentialErrorString) throws StandardException, SQLException {
        if (aSupplier == null) {
            throw new StandardException(StandardStatusEnum.NULL_INPUT_VALUE_NOT_ALLOWED, A_FUNCTION_FIELD);
        }

        StopWatch stopwatch = new StopWatch();

        if (log.isInfoEnabled()) {
            stopwatch.start();
        }
        try {
            S ret = aSupplier.get();

            if (log.isInfoEnabled()) {
                stopwatch.stop();

                log.info("{} ms for SELECT {}", stopwatch.getLastTaskTimeMillis(), aPotentialErrorString);
            }

            return ret;
        } catch (Exception aE) {
            log.error(String.format("SELECT '%s' failed", aPotentialErrorString), aE);
            Object[] argumentReference = new Object[]{aPotentialErrorString};
            throw new SQLException(SQLStatusEnum.DB_SELECT_FAILED_DESCRIPTION, aE, argumentReference);
        }
    }

    public static <T, S> S select(Function<T, S> aFunction, String aPotentialErrorString, T aInstance) throws StandardException, SQLException {
        if (aFunction == null) {
            throw new StandardException(StandardStatusEnum.NULL_INPUT_VALUE_NOT_ALLOWED, A_FUNCTION_FIELD);
        }

        try {
            StopWatch stopwatch = new StopWatch();

            if (log.isInfoEnabled()) {
                stopwatch.start();
            }
            S ret = aFunction.apply(aInstance);

            if (log.isInfoEnabled()) {
                stopwatch.stop();

                log.info("{} ms for SELECT {}", stopwatch.getLastTaskTimeMillis(), aPotentialErrorString);
            }
            return ret;
        } catch (Exception aE) {
            log.error(String.format("SELECT '%s' failed", aPotentialErrorString), aE);
            Object[] argumentReference = {aPotentialErrorString};
            throw new SQLException(SQLStatusEnum.DB_SELECT_FAILED_DESCRIPTION, aE, argumentReference);
        }
    }

    public static <T, S> S update(Function<T, S> aFunction, String aPotentialErrorString, T aInstance) throws StandardException, SQLException {
        if (aFunction == null) {
            throw new StandardException(StandardStatusEnum.NULL_INPUT_VALUE_NOT_ALLOWED, A_FUNCTION_FIELD);
        }

        try {
            StopWatch stopwatch = new StopWatch();

            if (log.isInfoEnabled()) {
                stopwatch.start();
            }

            S ret = aFunction.apply(aInstance);

            if (log.isInfoEnabled()) {
                stopwatch.stop();

                log.info("{} ms for UPDATE {}", stopwatch.getLastTaskTimeMillis(), aPotentialErrorString);
            }

            return ret;
        } catch (Exception aE) {
            log.error(String.format("UPDATE '%s' failed", aPotentialErrorString), aE);
            Object[] argumentReference = new Object[]{aPotentialErrorString};
            throw new SQLException(SQLStatusEnum.DB_UPDATE_FAILED_DESCRIPTION, aE, argumentReference);
        }
    }

    public static <T> void update(Consumer<T> aConsumer, String aPotentialErrorString, T aInstance) throws StandardException, SQLException {
        if (aConsumer == null) {
            throw new StandardException(StandardStatusEnum.NULL_INPUT_VALUE_NOT_ALLOWED, "aConsumer");
        }

        try {
            StopWatch stopwatch = new StopWatch();

            if (log.isInfoEnabled()) {
                stopwatch.start();
            }

            aConsumer.accept(aInstance);

            if (log.isInfoEnabled()) {
                stopwatch.stop();

                log.info("{} ms for UPDATE {}", stopwatch.getLastTaskTimeMillis(), aPotentialErrorString);
            }
        } catch (Exception aE) {
            log.error(String.format("UPDATE '%s' failed", aPotentialErrorString), aE);
            Object[] argumentReference = new Object[]{aPotentialErrorString};
            throw new SQLException(SQLStatusEnum.DB_UPDATE_FAILED_DESCRIPTION, aE, argumentReference);
        }
    }

    public static <T, S> S insert(Function<T, S> aFunction, String aPotentialErrorString, T aInstance) throws StandardException, SQLException {
        if (aFunction == null) {
            throw new StandardException(StandardStatusEnum.NULL_INPUT_VALUE_NOT_ALLOWED, A_FUNCTION_FIELD);
        }

        try {
            StopWatch stopwatch = new StopWatch();

            if (log.isInfoEnabled()) {
                stopwatch.start();
            }

            S ret = aFunction.apply(aInstance);

            if (log.isInfoEnabled()) {
                stopwatch.stop();

                log.info("{} ms for INSERT {}", stopwatch.getLastTaskTimeMillis(), aPotentialErrorString);
            }
            return ret;
        } catch (DuplicateKeyException aE) {
            log.error(String.format("INSERT '%s' failed with DuplicateKeyException", aPotentialErrorString), aE);
            throw new SQLException(SQLStatusEnum.DB_INSERT_FAILED_DUPLICATE_KEY, aE, aPotentialErrorString);
        } catch (Exception aE) {
            log.error(String.format("INSERT '%s' failed", aPotentialErrorString), aE);
            Object[] argumentReference = new Object[]{aPotentialErrorString};
            throw new SQLException(SQLStatusEnum.DB_INSERT_FAILED_DESCRIPTION, aE, argumentReference);
        }
    }

    public static <T> void insert(Consumer<T> aFunction, String aPotentialErrorString, T aInstance) throws StandardException, SQLException {
        if (aFunction == null) {
            throw new StandardException(StandardStatusEnum.NULL_INPUT_VALUE_NOT_ALLOWED, A_FUNCTION_FIELD);
        }

        try {
            StopWatch stopwatch = new StopWatch();

            if (log.isInfoEnabled()) {
                stopwatch.start();
            }

            aFunction.accept(aInstance);

            if (log.isInfoEnabled()) {
                stopwatch.stop();

                log.info("{} ms for INSERT {}", stopwatch.getLastTaskTimeMillis(), aPotentialErrorString);
            }
        } catch (DuplicateKeyException aE) {
            log.error(String.format("INSERT '%s' failed with DuplicateKeyException", aPotentialErrorString), aE);
            throw new SQLException(SQLStatusEnum.DB_INSERT_FAILED_DUPLICATE_KEY, aE, aPotentialErrorString);
        } catch (Exception aE) {
            log.error(String.format("INSERT '%s' failed", aPotentialErrorString), aE);
            Object[] argumentReference = new Object[]{aPotentialErrorString};
            throw new SQLException(SQLStatusEnum.DB_INSERT_FAILED_DESCRIPTION, aE, argumentReference);
        }
    }
    
    public static <T, S> S delete(Function<T, S> aFunction, String aPotentialErrorString, T aInstance) throws StandardException, SQLException {
        if (aFunction == null) {
            throw new StandardException(StandardStatusEnum.NULL_INPUT_VALUE_NOT_ALLOWED, A_FUNCTION_FIELD);
        }
        try {
            StopWatch stopWatch = new StopWatch();
            
            if (log.isInfoEnabled()) {
                stopWatch.start();
            }
            S ret = aFunction.apply(aInstance);
            
            if (log.isInfoEnabled()) {
                stopWatch.stop();
                log.info("{} ms for DELETE {}", stopWatch.getLastTaskTimeMillis(), aPotentialErrorString);
            }
            return ret;
        } catch(Exception aE) {
            log.error(String.format("DELETE '%s' failed", aPotentialErrorString), aE);
            Object[] argumentReference = new Object[]{aPotentialErrorString};
            throw new SQLException(SQLStatusEnum.DB_DELETE_FAILED_DESCRIPTION, aE, argumentReference);
        }
    }
    
    public static <T> void delete(Consumer<T> aConsumer, String aPotentialErrorString, T aInstance) throws StandardException, SQLException {
        if (aConsumer == null) {
            throw new StandardException(StandardStatusEnum.NULL_INPUT_VALUE_NOT_ALLOWED, "aConsumer");
        }

        try {
            StopWatch stopwatch = new StopWatch();

            if (log.isInfoEnabled()) {
                stopwatch.start();
            }

            aConsumer.accept(aInstance);

            if (log.isInfoEnabled()) {
                stopwatch.stop();

                log.info("{} ms for DELETE {}", stopwatch.getLastTaskTimeMillis(), aPotentialErrorString);
            }
        } catch (Exception aE) {
            log.error(String.format("DELETE '%s' failed", aPotentialErrorString), aE);
            Object[] argumentReference = new Object[]{aPotentialErrorString};
            throw new SQLException(SQLStatusEnum.DB_DELETE_FAILED_DESCRIPTION, aE, argumentReference);
        }
    }

}
