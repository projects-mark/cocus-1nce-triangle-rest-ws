package pt.cocus.nce.rest.ws.service.abstractions;

import java.util.List;
import java.util.Optional;
import pt.cocus.nce.rest.ws.core.exceptions.SQLException;
import pt.cocus.nce.rest.ws.core.exceptions.StandardException;
import pt.cocus.nce.rest.ws.model.dto.request.TriangleClassificationRequest;
import pt.cocus.nce.rest.ws.model.dto.response.TriangleClassificationQueryResponse;
import pt.cocus.nce.rest.ws.model.dto.response.TriangleClassificationResponse;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 */
public interface TriangleClassificationServiceAbstraction {
    
    public static final String BEAN_NAME = "service.TriangleClassificationServiceAbstraction";
    
    TriangleClassificationResponse processTriangleClassificationQuery(final TriangleClassificationRequest request) throws StandardException, SQLException;
    List<TriangleClassificationQueryResponse> queryTriangleClassificationHistories() throws StandardException, SQLException;
    List<TriangleClassificationQueryResponse> queryTriangleClassificationHistoriesViaUsername(final String username) throws StandardException, SQLException;
    Optional<TriangleClassificationQueryResponse> queryTriangleClassificationHistory(final String referenceNumber) throws StandardException, SQLException;

}
