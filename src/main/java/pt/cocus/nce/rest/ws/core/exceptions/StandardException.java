package pt.cocus.nce.rest.ws.core.exceptions;

import pt.cocus.nce.rest.ws.model.enums.BaseStatusEnum;
import pt.cocus.nce.rest.ws.model.enums.StandardStatusEnum;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 * Exception class used for SQL exceptions
 */
public class StandardException extends BaseException {
 
	private static final long serialVersionUID = -3259203758714333741L;

	public StandardException(final String message) {
        super(message);
    }

    public StandardException(final Throwable e) {
        super(e);
    }

    public StandardException(final String message, final Throwable e) {
        super(message, e);
    }

    public StandardException(final StandardStatusEnum aPredefinedError) {
        super(aPredefinedError);
    }

    public StandardException(final StandardStatusEnum aPredefinedError, final Throwable e) {
        super(aPredefinedError, e);
    }

    public StandardException(final StandardStatusEnum aPredefinedError, final Throwable e, final String aAdditionalInfo) {
        super(aPredefinedError, e, aAdditionalInfo);
    }

    public StandardException(final StandardStatusEnum aPredefinedError, final String aAdditionalInfo) {
        super(aPredefinedError, aAdditionalInfo);
    }

    public StandardException(final StandardStatusEnum aPredefinedError, final Throwable e, final Object... aSubstitutionValues) {
        super(aPredefinedError, e, aSubstitutionValues);
    }

    public StandardException(final StandardStatusEnum aPredefinedError, final Object...aSubstitutionValues) {
        super(aPredefinedError, aSubstitutionValues);
    }

    public static Class<? extends BaseStatusEnum<?>> getPredefinedStatusType() throws BaseException {
        return StandardStatusEnum.class;
    }
}
