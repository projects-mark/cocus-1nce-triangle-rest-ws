package pt.cocus.nce.rest.ws.core.utils;

import java.util.function.Function;

import org.apache.commons.lang3.ArrayUtils;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
public class GetterUtil {

	private GetterUtil() {}
    /**
     * Returns the getter value from the instance using the specified function, if the instance is not null, else returns null
     */
    public static <T, R> R safeGet(T instance, Function<T, R> aFunction) {
        if (instance == null) {
            return null;
        } else {
            return aFunction.apply(instance);
        }
    }

    /**
     * Returns the getter value from the instance using the specified function, if the instance is not null, in sequence until the last function is reached or we get a null value, else returns null
     */
    public static <T, R, S> S safeGet(T instance, Function<T, R> aFunction, Function<R, S> aFunctionB) {
        if (instance == null) {
            return null;
        } else {
            return safeGet(aFunction.apply(instance), aFunctionB);
        }
    }

    /**
     * Returns the getter value from the instance using the specified function, if the instance is not null, in sequence until the last function is reached or we get a null value, else returns null
     */
    public static <T, R, S, U> U safeGet(T instance, Function<T, R> aFunction, Function<R, S> aFunctionB, Function<S, U> aFunctionC) {
        if (instance == null) {
            return null;
        } else {
            return safeGet(aFunction.apply(instance), aFunctionB, aFunctionC);
        }
    }

    /**
     * Returns the getter value from the instance using the specified function, if the instance is not null, in sequence until the last function is reached or we get a null value, else returns null
     */
    public static <T, R, S, U, V> V safeGet(T instance, Function<T, R> aFunction, Function<R, S> aFunctionB, Function<S, U> aFunctionC, Function<U, V> aFunctionD) {
        if (instance == null) {
            return null;
        } else {
            return safeGet(aFunction.apply(instance), aFunctionB, aFunctionC, aFunctionD);
        }
    }

    /**
     * Returns the getter value from the instance using the specified function, if the instance is not null, in sequence until the last function is reached or we get a null value, else returns null
     */
    public static <T, R, S, U, V, W> W safeGet(T instance, Function<T, R> aFunction, Function<R, S> aFunctionB, Function<S, U> aFunctionC, Function<U, V> aFunctionD, Function<V, W> aFunctionE) {
        if (instance == null) {
            return null;
        } else {
            return safeGet(aFunction.apply(instance), aFunctionB, aFunctionC, aFunctionD, aFunctionE);
        }
    }

    /**
     * Verifies that neither the instance or any of the specified fields are null
     */
    @SafeVarargs
	public static <T> boolean noneNull(T instance, Function<T, ?>... aFunctions) {
        if (instance == null) {
            return false;
        }

        for (Function<T, ?> function : aFunctions) {
            if (function.apply(instance) == null) {
                return false;
            }
        }

        return true;
    }

    @SafeVarargs
	public static <T, S> S safeGetFirstNonNull(Function<T, S> aGetter, T... aInstanceArray) {
        return safeGetFirstNonNullOrDefault(aGetter, null, aInstanceArray);
    }

    @SafeVarargs
	public static <T, S> S safeGetFirstNonNullOrDefault(Function<T, S> aGetter, S aDefaultValue, T... aInstanceArray) {
        if (ArrayUtils.isEmpty(aInstanceArray) || aGetter == null) {
            return aDefaultValue;
        }

        S value;

        for (T instance : aInstanceArray) {
            if (instance == null) {
                continue;
            }

            value = aGetter.apply(instance);

            if (value != null) {
                return value;
            }
        }

        return aDefaultValue;
    }
    
    /**
     * Coalescing utility, to using ternary under the hood.
     */
    public static <T> T coalesce(T leftHand, T rightHand) {
        return leftHand != null ? leftHand : rightHand;
    }
    
}
