package pt.cocus.nce.rest.ws.config;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
@Configuration
@EnableSwagger2
public class SpringfoxConfig {
    
    @Autowired
    @Qualifier(value = SpringfoxConfigProperties.BEAN_NAME)
    private SpringfoxConfigProperties config;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("pt.cocus.nce.rest.ws.web.api"))
                .paths(PathSelectors.any()).build().apiInfo(apiInfo())
                .securityContexts(Arrays.asList(securityContext()))
                .securitySchemes(Arrays.asList(apiKey()));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title(config.getTitle()).description(config.getDescription())
                .termsOfServiceUrl(config.getTermsOfServiceUrl()).contact(new Contact(config.getContactName(), config.getContactUrl(), config.getContactEmail()))
                .license(config.getLicense())
                .licenseUrl(config.getLicenseUrl()).version(config.getVersion()).build();
    }

    private SecurityContext securityContext(){
        return SecurityContext.builder().securityReferences(defaultAuth()).build();
    }
    
    private ApiKey apiKey() { 
        return new ApiKey("Gateway Barrier", "Authorization", "header"); 
    }

    private List<SecurityReference> defaultAuth(){
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("Gateway Barrier", authorizationScopes));
    }
}
