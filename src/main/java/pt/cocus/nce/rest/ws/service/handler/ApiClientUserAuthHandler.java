package pt.cocus.nce.rest.ws.service.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import pt.cocus.nce.rest.ws.core.exceptions.SQLException;
import pt.cocus.nce.rest.ws.core.exceptions.StandardException;
import pt.cocus.nce.rest.ws.core.utils.JdbcRequestUtil;
import pt.cocus.nce.rest.ws.model.enums.Role;
import pt.cocus.nce.rest.ws.model.enums.StandardStatusEnum;
import pt.cocus.nce.rest.ws.service.abstractions.ApiClientUserAuthServiceAbstraction;
import pt.cocus.nce.triangle.domain.dao.ApiClientUserDao;
import pt.cocus.nce.triangle.domain.dao.query.ApiClientUserQuery;
import pt.cocus.nce.triangle.domain.entity.ApiClientUserEntity;

/**
 * @author mark-mngoma
 * @created_at 30 Jan 2022
 */
@Service(value = ApiClientUserAuthServiceAbstraction.BEAN_NAME)
public class ApiClientUserAuthHandler implements ApiClientUserAuthServiceAbstraction {

    private static final Logger log = LoggerFactory.getLogger(ApiClientUserAuthHandler.class);
    
    @Autowired
    @Qualifier(value = ApiClientUserDao.BEAN_NAME) 
    private ApiClientUserDao apiClientUserDao;
    
    @Override
    public UserDetails queryUserViaUsername(final String username)
            throws StandardException, SQLException {
        ApiClientUserQuery query = new ApiClientUserQuery();
        query.setApiClientUserLogin(username);
        UserDetails validatedUser = null;
        final Optional<ApiClientUserEntity> userEntity =
                JdbcRequestUtil.select(apiClientUserDao::queryClientByUsername,
                        "apiClientUserDao::queryClientByUsername", query);
        List<Role> roles = new ArrayList<>();
        roles.add(Role.ROLE_API_CLIENT);
        if (userEntity.isPresent()) {
            final ApiClientUserEntity entity = userEntity.get();
            validatedUser = User.withUsername(entity.getLogin()).password(entity.getPassword())
                    .authorities(roles).accountExpired(false).accountLocked(false)
                    .credentialsExpired(false).disabled(false).build();
        } else {
            log.error("No RECORDS found for this user: #{}", username);
            throw new StandardException(StandardStatusEnum.NO_DATA_FOUND,
                    new Object[] {"userEntity"});
        }
        return validatedUser;
    }
}
