package pt.cocus.nce.rest.ws.model.enums;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 * 
 * Defines the list of status values for use with SQL statements / MyBatis
 */
public enum SQLStatusEnum implements BaseStatusEnum<SQLStatusEnum> {

	DB_CONNECT_FAILED(10101, "Failed to connect to database", 422), 
	DB_UNAVAILABLE(10102, "Database unavailable", 422),
	DB_UPDATE_FAILED(10103, "Database update failed", 422), 
	DB_INSERT_FAILED(10104, "Database insert failed", 422),
	DB_SELECT_FAILED(10105, "Database select failed", 422),
	DB_BATCH_UPDATE_FAILED(10106, "Database batch update failed", 422),
	DB_BATCH_INSERT_FAILED(10107, "Database batch insert failed", 422),
	DB_UNEXPECTED_AFFECTED_ROWS(10108,
			"Unexpected number of rows affected, expected %d rows to be affected, but %d were affected.", 422),
	DB_UPDATE_FAILED_DESCRIPTION(10109, "Database update failed for '%s'", 422),
	DB_INSERT_FAILED_DESCRIPTION(10110, "Database insert failed for '%s'", 422),
	DB_SELECT_FAILED_DESCRIPTION(10111, "Database select failed for '%s'", 422),
	DB_BATCH_UPDATE_FAILED_DESCRIPTION(10112, "Database batch update failed for '%s'", 422),
	DB_BATCH_INSERT_FAILED_DESCRIPTION(10113, "Database batch insert failed for '%s'", 422),
	DB_BATCH_SELECT_FAILED_DESCRIPTION(10114, "Database batch select failed for '%s'", 422),
	DB_DELETE_FAILED(10115, "Database delete failed", 422),
	UPDATE_ALREADY_IN_PROGRESS_FOR(10116, "Update already in progress for %s", 422),
	UPDATED_LESS_ENTRIES_THAN_EXPECTED(10117,
			"Update less entries than expected for %s, expected %d but only updated %d", 422),
	UPDATED_MORE_ENTRIES_THAN_EXPECTED(10118,
			"Update less entries than expected for %s, expected %d but only updated %d", 422),
	MERGE_INSERT_FAILED(10119, "Merge insert failed for %s", 422),
	DB_INSERT_FAILED_DUPLICATE_KEY(10120, "Database insert '%s' failed due to duplicate key constraint", 422),
	DB_DELETE_FAILED_DESCRIPTION(10121, "Database delete failed for '%s'", 422);

	final int errorCode;
	final String statusDescription;
	final int httpResponseCode;
	final boolean successStatus;

	SQLStatusEnum(final int aErrorCode, final String aStatusDescription, final int aHTTPResponseCode) {
		this(aErrorCode, aStatusDescription, aHTTPResponseCode, false);
	}

	SQLStatusEnum(final int aErrorCode, final String aStatusDescription, final int aHTTPResponseCode,
			final boolean aSuccessStatus) {
		errorCode = aErrorCode;
		statusDescription = aStatusDescription;
		successStatus = aSuccessStatus;
		httpResponseCode = aHTTPResponseCode;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public int getCodeRangeMin() {
		return 10100;
	}

	public int getCodeRangeMax() {
		return 10199;
	}

	public int getHttpResponseCode() {
		return httpResponseCode;
	}

	public boolean isSuccessStatus() {
		return successStatus;
	}

	@Override
	public String toStandardString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public SQLStatusEnum valueOf() {
		return valueOf(name());
	}

	@Override
	public String getFormatted(Object... aParameters) {
		return String.format(getStatusDescription(), aParameters);
	}

}
