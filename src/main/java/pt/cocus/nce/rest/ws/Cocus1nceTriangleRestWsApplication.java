package pt.cocus.nce.rest.ws;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "pt.cocus.nce.rest.ws, ")
@MapperScan(basePackages = { "pt.cocus.nce.triangle.domain" })
public class Cocus1nceTriangleRestWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Cocus1nceTriangleRestWsApplication.class, args);
	}

}
