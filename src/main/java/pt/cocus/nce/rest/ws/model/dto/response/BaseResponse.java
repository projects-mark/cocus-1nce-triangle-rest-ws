package pt.cocus.nce.rest.ws.model.dto.response;

import java.io.Serializable;
import org.springframework.http.HttpStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class BaseResponse implements Serializable {

    private static final long serialVersionUID = -2876479044673681812L;
    
    private boolean isValidRequest;
    private HttpStatus statusCode; 
    private String message;
    private Object result;

    public boolean isValidRequest() {
        return isValidRequest;
    }

    public void setValidRequest(boolean isValidRequest) {
        this.isValidRequest = isValidRequest;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
        
}