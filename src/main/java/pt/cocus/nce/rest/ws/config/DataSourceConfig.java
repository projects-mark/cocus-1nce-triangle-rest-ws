package pt.cocus.nce.rest.ws.config;

import java.io.IOException;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.Location;
import org.flywaydb.core.api.configuration.ClassicConfiguration;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
@Configuration(value = DataSourceConfig.BEAN_NAME)
public class DataSourceConfig {

    public static final String BEAN_NAME = "config.DataSourceConfig";

    @Value("${flyway.license-key}")
    private String licenseKey;
    
    private final DataSourceConfigProperties config;
    private final HikariConnectionProperties hikariConnectionProperties;
    
    @Autowired
    public DataSourceConfig(@Qualifier(value = DataSourceConfigProperties.BEAN_NAME) final DataSourceConfigProperties config, 
                            final HikariConnectionProperties hikariConnectionProperties) {
        this.config = config;
        this.hikariConnectionProperties = hikariConnectionProperties;
    }
    
    @Bean(destroyMethod = "close", value = "dataSource")
    public HikariDataSource dataSource() {
        return new HikariDataSource(hikariConnectionPoolConfig());
    }
    
    @Bean("hikariConnectionPool")
    public HikariConfig hikariConnectionPoolConfig() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(config.getDriver());
        hikariConfig.setJdbcUrl(config.getUrl());
        hikariConfig.setUsername(config.getUsername());
        hikariConfig.setPassword(config.getPassword());
        
        hikariConfig.setMaximumPoolSize(hikariConnectionProperties.getMaximumPoolSize());
        hikariConfig.setMaxLifetime(hikariConnectionProperties.getMaxLifetime());
        hikariConfig.setIdleTimeout(hikariConnectionProperties.getIdleTimeout());
        hikariConfig.setLeakDetectionThreshold(hikariConnectionProperties.getLeakDetectionThreshold());
        hikariConfig.setConnectionTimeout(hikariConnectionProperties.getConnectionTimeout());
        hikariConfig.setConnectionTestQuery(hikariConnectionProperties.getConnectionTestQuery());
        hikariConfig.setAutoCommit(hikariConnectionProperties.isAutoCommit());
        return hikariConfig;
    }
    
    
    @Bean(value = "migrationBean", initMethod = "migrate")
    public Flyway flywayMigrationConfig() {
        ClassicConfiguration flywayUbiquitousConfig = new ClassicConfiguration();
        flywayUbiquitousConfig.setDataSource(dataSource());
        flywayUbiquitousConfig.setLocations(new Location(config.getMigrationScriptPath()));
        flywayUbiquitousConfig.setValidateMigrationNaming(config.isValidateMigrationNaming());
        flywayUbiquitousConfig.setValidateOnMigrate(config.isValidateOnMigrate());
        flywayUbiquitousConfig.setLicenseKey(licenseKey);
        return new Flyway(flywayUbiquitousConfig);
    }
    
    @Bean
    public SqlSessionFactoryBean sqlSessionFactory() throws IOException {
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource());
        sessionFactoryBean.setConfigLocation(new ClassPathResource(config.getMapperConfigPath()));
        sessionFactoryBean.setTypeAliasesPackage(config.getEntityPackage());
        sessionFactoryBean.setMapperLocations(resolver.getResources(config.getMapperLocation()));
        return sessionFactoryBean;
    }
    
    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }
  
}
