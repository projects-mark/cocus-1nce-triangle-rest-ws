package pt.cocus.nce.rest.ws.core.exceptions;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pt.cocus.nce.rest.ws.model.enums.BaseStatusEnum;
import pt.cocus.nce.rest.ws.model.enums.StandardStatusEnum;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 * Base exception class
 */
public class BaseException extends Exception {

	private static final long serialVersionUID = -3440309606916268528L;

	private Logger log;

    private BaseStatusEnum<?> predefinedStatus;
    private boolean overrideStatus;
    private String additionalInfo;
    private Object[] substitutionValues;

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public Object[] getSubstitutionValues() {
        return substitutionValues;
    }

    public void setAdditionalInfo(String aAdditionalInfo) {
        additionalInfo = aAdditionalInfo;
    }

    public BaseStatusEnum<?> getPredefinedStatus() {
        return predefinedStatus;
    }

    public <T extends BaseStatusEnum<?>> T getPredefinedStatus(Class<T> aClass) {
        return aClass.cast(predefinedStatus);
    }

    /**
     * Override status supplied when throwing the exception
     */
    public boolean isOverrideStatus() {
        return overrideStatus;
    }

    public void setOverrideStatus(final boolean aOverrideStatus) {
        overrideStatus = aOverrideStatus;
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(Throwable e) {
        super(e);
    }

    public BaseException(String message, Throwable e) {
        super(message, e);
    }

    public <T extends BaseStatusEnum<?>> BaseException(T aPredefinedError) {
        super();
        predefinedStatus = aPredefinedError;
    }

    public <T extends BaseStatusEnum<?>> BaseException(T aPredefinedError, Throwable e) {
        super(e);
        predefinedStatus = aPredefinedError;
    }

    public <T extends BaseStatusEnum<?>> BaseException(T aPredefinedError, Throwable e, Object... aSubstitutionValues) {
        super(e);
        init(aPredefinedError, aSubstitutionValues);
    }

    public <T extends BaseStatusEnum<?>> BaseException(T aPredefinedError, Object... aSubstitutionValues) {
        super();
        init(aPredefinedError, aSubstitutionValues);
    }

    private <T extends BaseStatusEnum<?>> void init(final T aPredefinedError, final Object... aSubstitutionValues) {
        predefinedStatus = aPredefinedError;

        if (aSubstitutionValues != null
                && aSubstitutionValues.length == 1
                && aPredefinedError != null
                && !aPredefinedError.getStatusDescription().contains("%")) {
            if (aSubstitutionValues[0] instanceof String) {
                additionalInfo = (String) aSubstitutionValues[0];
            } else {
                additionalInfo = String.valueOf(aSubstitutionValues[0]);
            }
        } else {
            substitutionValues = aSubstitutionValues;
        }
    }

    protected Logger getLogger() {
        if (log == null) {
            log = LoggerFactory.getLogger(this.getClass());
        }

        return log;
    }

    @Override
    public String getMessage() {
        if (getPredefinedStatus() != null) {
            if (additionalInfo != null && additionalInfo.trim().length() > 0) {
                return String.format("%s: %s", getPredefinedStatus().getStatusDescription(), additionalInfo);
            } else if (substitutionValues != null && substitutionValues.length > 0) {
                try {
                    return String.format(getPredefinedStatus().getStatusDescription(), substitutionValues);
                } catch (Exception e) {
                    if (getLogger().isErrorEnabled()) {
                        getLogger().error("Unable to format input String '{}' ' with parameters [{}]", getPredefinedStatus() != null ? getPredefinedStatus().getStatusDescription() : null, substitutionValues, e);
                    }

                    BaseStatusEnum<?> status = getPredefinedStatus() != null ? getPredefinedStatus() : StandardStatusEnum.UNDEFINED_STATUS;

                    return String.format("%s: %s - %s",
                            status,
                            status.getStatusDescription(),
                            ToStringBuilder.reflectionToString(substitutionValues));
                }
            } else {
                return getPredefinedStatus().getStatusDescription();
            }
        } else if (additionalInfo != null) {
            if (super.getMessage() != null) {
                return String.format("%s: %s", super.getMessage(), additionalInfo);
            } else {
                return additionalInfo;
            }
        } else {
            return super.getMessage();
        }
    }

    /**
     * This is meant to block the process when looking for a root cause of an exception
     * <p>
     * This method considers the status set on the Exception as well as the status set on the enum value
     */
    public boolean isOverallOverrideStatus() {
        return isOverrideStatus() || (getPredefinedStatus() != null && getPredefinedStatus().isOverrideStatus());
    }

    @Override
    public String toString() {
        if (predefinedStatus == null) {
            return super.toString();
        } else {
            return getMessage();
        }
    }

    public static Class<? extends BaseStatusEnum<?>> getPredefinedStatusType() throws BaseException {
        throw new BaseException("PredefinedStatusClass not defined");
    }
}

