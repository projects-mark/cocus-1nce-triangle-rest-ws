package pt.cocus.nce.rest.ws.model.enums;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 * 
 * Specifies the standard application errors
 */
public enum StandardStatusEnum implements BaseStatusEnum<StandardStatusEnum> {

    SUCCESS(1001, "Success", 200, true),
    UNHANDLED_EXCEPTION(1002, "Unhandled exception", 500),
    UNHANDLED_RUNTIME_EXCEPTION(1003, "Unhandled runtime exception of type '%s'", 500),
    INVALID_INPUT_PARAMETER(1004, "Invalid input for parameter '%s', value '%s'", 422),
    NULL_INPUT_VALUE_NOT_ALLOWED(1005, "Null input not allowed for parameter '%s'", 422),
    INVALID_INPUT_TYPE(1006, "Invalid input type '%s' for parameter '%s', expected type '%s'", 422),
    INVALID_INPUT_FIELD_LENGTH(1007, "Invalid field length for input field '%s', expected length is %d but found %d ('%s')", 422),
    COULD_NOT_FORMAT_ERROR_STRING(1008, "Could not format error string: %s with variables %s", 422),
    NO_CONTENT(1009, "No Content", 204),
    EITHER_OR_PARAMETER_NOT_SUPPLIED(1010, "Either '%s' or '%s' need to be provided", 422),
    BOTH_PARAMETERS_NOT_PERMITTED(1011, "Can't have both '%s' and '%s' provided at the same time", 422),
    NO_DATA_FOUND(1012, "No data was found", 404),
    CANT_HAVE_BOTH_FIELDS_NULL(1013, "Both parameters cannot be null: '%s' and '%s' ", 422),
    UNDEFINED_STATUS(1014, "UNDEFINED_STATUS", 500, true);
    
    final int errorCode;
    final String statusDescription;
    final int httpResponseCode;
    final boolean successStatus;

    StandardStatusEnum(final int aErrorCode, final String aStatusDescription, final int aHTTPResponseCode) {
        this(aErrorCode, aStatusDescription, aHTTPResponseCode, false);
    }

    StandardStatusEnum(final int aErrorCode, final String aStatusDescription, final int aHTTPResponseCode,
            final boolean aSuccessStatus) {
        errorCode = aErrorCode;
        statusDescription = aStatusDescription;
        successStatus = aSuccessStatus;
        httpResponseCode = aHTTPResponseCode;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public int getCodeRangeMin() {
        return 10000;
    }

    public int getCodeRangeMax() {
        return 10099;
    }

    public int getHttpResponseCode() {
        return httpResponseCode;
    }

    public boolean isSuccessStatus() {
        return successStatus;
    }

    @Override
    public String toStandardString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public StandardStatusEnum valueOf() {
        return valueOf(name());
    }

    @Override
    public String getFormatted(Object... aParameters) {
        return String.format(getStatusDescription(), aParameters);
    }
}

