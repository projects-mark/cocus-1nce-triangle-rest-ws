package pt.cocus.nce.rest.ws.model.dto.request;

import java.io.Serializable;
import javax.validation.constraints.Min;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 */
public class TriangleClassificationRequest implements Serializable {

    private static final long serialVersionUID = -7866053757889290160L;
    
    @Min(value = 1, message = "To perform triangle calculation, value of X must be > 0")
    @JsonProperty(value = "point_x", required = true)
    @ApiModelProperty(example = "2", required = true)
    private int pointX;
    
    @Min(value = 1, message = "To perform triangle calculation, value of Y must be > 0")
    @JsonProperty(value = "point_y", required = true)
    @ApiModelProperty(example = "6", required = true)
    private int pointY;
    
    @Min(value = 1, message = "To perform triangle calculation, value of Z must be > 0")
    @JsonProperty(value = "point_z", required = true)
    @ApiModelProperty(example = "9", required = true)
    private int pointZ;
    
    public int getPointX() {
        return pointX;
    }
    public void setPointX(int pointX) {
        this.pointX = pointX;
    }
    public int getPointY() {
        return pointY;
    }
    public void setPointY(int pointY) {
        this.pointY = pointY;
    }
    public int getPointZ() {
        return pointZ;
    }
    public void setPointZ(int pointZ) {
        this.pointZ = pointZ;
    }
    
}
