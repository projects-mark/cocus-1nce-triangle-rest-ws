package pt.cocus.nce.rest.ws.model.dto.request;

import java.io.Serializable;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
public class AuthenticationLogonRequest implements Serializable {

    private static final long serialVersionUID = 7479100712999142796L;
    
    @NotEmpty
    @Email(message = "Username requires an email address.")
    @ApiModelProperty(required = true, example = "cocus.user@1nce.com")
    private String username;
    
    @NotEmpty(message = "Please enter a password longer than 5 characters")
    @Size(min = 5, message = "Your password is too short, should be more than 5 characters")
    @ApiModelProperty(required = true, example = "s3cretPassword")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
