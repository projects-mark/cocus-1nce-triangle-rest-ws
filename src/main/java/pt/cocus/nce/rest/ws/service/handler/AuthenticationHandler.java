package pt.cocus.nce.rest.ws.service.handler;

import java.util.Arrays;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pt.cocus.nce.rest.ws.config.jwt.JwtAuthenicationManager;
import pt.cocus.nce.rest.ws.config.jwt.JwtTokenProvider;
import pt.cocus.nce.rest.ws.core.exceptions.SQLException;
import pt.cocus.nce.rest.ws.core.exceptions.StandardException;
import pt.cocus.nce.rest.ws.core.utils.GetterUtil;
import pt.cocus.nce.rest.ws.core.utils.JdbcRequestUtil;
import pt.cocus.nce.rest.ws.model.dto.request.AuthenticationLogonRequest;
import pt.cocus.nce.rest.ws.model.dto.request.AuthenticationRegisterRequest;
import pt.cocus.nce.rest.ws.model.dto.response.AuthenticationResponse;
import pt.cocus.nce.rest.ws.model.enums.Role;
import pt.cocus.nce.rest.ws.model.enums.StandardStatusEnum;
import pt.cocus.nce.rest.ws.service.abstractions.AuthenticationServiceAbstraction;
import pt.cocus.nce.triangle.domain.dao.ApiClientUserDao;
import pt.cocus.nce.triangle.domain.dao.query.ApiClientUserQuery;
import pt.cocus.nce.triangle.domain.entity.ApiClientUserEntity;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
@Service(value = AuthenticationServiceAbstraction.BEAN_NAME)
public class AuthenticationHandler implements AuthenticationServiceAbstraction {

    private static final Logger log = LoggerFactory.getLogger(AuthenticationHandler.class);
    
    @Value(value = "${application.messages.register-greeting}")
    private String ON_BOARD_USER_SUCCESS_MESSAGE;
    
    private final ApiClientUserDao apiClientUserDao;
    
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private JwtAuthenicationManager authenticationManager;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    public AuthenticationHandler(@Qualifier(value = ApiClientUserDao.BEAN_NAME) final ApiClientUserDao apiClientUserDao,
                                PasswordEncoder passwordEncoder) {
        this.apiClientUserDao = apiClientUserDao;
    }
    
    @Override
    public AuthenticationResponse processOnboarding(final AuthenticationRegisterRequest request) throws StandardException, SQLException {
        AuthenticationResponse response = new AuthenticationResponse();
        ApiClientUserQuery query = new ApiClientUserQuery();
        query.setApiClientUserLogin(GetterUtil.safeGet(request, AuthenticationRegisterRequest::getUsername));
        final Optional<ApiClientUserEntity> clientUserEntity = queryValidUsername(query);
        if (clientUserEntity.isPresent() && (clientUserEntity.get() != null)) {
            response.setValidRequest(false);
            response.setStatusCode(HttpStatus.UNPROCESSABLE_ENTITY);
            response.setMessage("Seems like this user already exists!");
            return response;
        }
        ApiClientUserEntity entity = new ApiClientUserEntity();
        entity.setLogin(GetterUtil.safeGet(request, AuthenticationRegisterRequest::getUsername));
        entity.setPassword(passwordEncoder.encode(GetterUtil.safeGet(request, AuthenticationRegisterRequest::getPassword)));
        JdbcRequestUtil.insert(apiClientUserDao::insertApiClientUser, "apiClientUserDao::insertApiClientUser", entity);
        response.setValidRequest(true);
        response.setSuccessMessage(String.format(ON_BOARD_USER_SUCCESS_MESSAGE, GetterUtil.safeGet(request, AuthenticationRegisterRequest::getUsername)));
        response.setStatusCode(response.isValidRequest() ? HttpStatus.CREATED : HttpStatus.UNPROCESSABLE_ENTITY);
        return response;
    }
    
    @Override
    public AuthenticationResponse processSignOn(final AuthenticationLogonRequest logonRequest)
            throws StandardException, SQLException {
        AuthenticationResponse response = new AuthenticationResponse();
        try {
            Authentication userToken = new UsernamePasswordAuthenticationToken(
                    logonRequest.getUsername(), logonRequest.getPassword());
            Authentication authResult = authenticationManager.authenticate(userToken);

            SecurityContext sc = SecurityContextHolder.getContext();
            sc.setAuthentication(authResult);
            
            ApiClientUserQuery query = new ApiClientUserQuery();
            query.setApiClientUserLogin(GetterUtil.safeGet(logonRequest, AuthenticationLogonRequest::getUsername));
            final Optional<ApiClientUserEntity> entity = queryValidUsername(query);
            
            if (entity.isPresent() && entity.get() != null) {
                response.setToken(jwtTokenProvider.constructToken(logonRequest.getUsername(),
                        Arrays.asList(Role.values())));
            }
            response.setValidRequest(true);
        } catch (BadCredentialsException aE) {
            log.error("Credentials Provided are not valid/insufficient {}, {}", aE.getMessage(), logonRequest.getClass().getCanonicalName());
            throw new StandardException(StandardStatusEnum.NO_DATA_FOUND,
                    new Object[] {"bad credentials"});
        }
        response.setStatusCode(response.isValidRequest() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
        return response;
    }

    private Optional<ApiClientUserEntity> queryValidUsername(final ApiClientUserQuery query) throws StandardException, SQLException {
        return JdbcRequestUtil.select(apiClientUserDao::queryClientByUsername, "apiClientUserDao::queryClientByUsername", query);
    }
}
