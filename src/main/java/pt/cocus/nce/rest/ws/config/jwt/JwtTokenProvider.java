package pt.cocus.nce.rest.ws.config.jwt;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import pt.cocus.nce.rest.ws.core.exceptions.SQLException;
import pt.cocus.nce.rest.ws.core.exceptions.StandardException;
import pt.cocus.nce.rest.ws.model.enums.Role;
import pt.cocus.nce.rest.ws.model.enums.StandardStatusEnum;
import pt.cocus.nce.rest.ws.service.abstractions.ApiClientUserAuthServiceAbstraction;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
@Component
public class JwtTokenProvider {
    
    private static final Logger log = LoggerFactory.getLogger(JwtTokenProvider.class);
    
    @PostConstruct
    protected void init() {
        JWT_SECRET =  Base64.getEncoder().encodeToString(JWT_SECRET.getBytes());
    }
    
    private static final String AUTH_HEADER = "Authorization";
    private static final String BEARER = "Bearer ";
    
    @Value("${jwt.secret}")
    private String JWT_SECRET;
    
    @Value("${jwt.token.ttl-ms}")
    private long JWT_TOKEN_TTL;

    private final ApiClientUserAuthServiceAbstraction abstractApiClientUserAuthHandler;
    
    @Autowired
    public JwtTokenProvider(@Qualifier(value = ApiClientUserAuthServiceAbstraction.BEAN_NAME) final ApiClientUserAuthServiceAbstraction abstractApiClientUserAuthHandler) {
        this.abstractApiClientUserAuthHandler = abstractApiClientUserAuthHandler;
    }

    public String constructToken(String username, List<Role> roles) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("auth", roles.stream().map(val -> new SimpleGrantedAuthority(val.getAuthority())).filter(Objects::nonNull).collect(Collectors.toList()));
        LocalDateTime currentIssueTimestamp = LocalDateTime.now();
        LocalDateTime validityPeriod = currentIssueTimestamp.plusSeconds(TimeUnit.MILLISECONDS.toSeconds(JWT_TOKEN_TTL));
        return Jwts.builder()
                   .setClaims(claims)
                   .setIssuedAt(Date.from(currentIssueTimestamp.atZone(ZoneId.systemDefault()).toInstant()))
                   .setExpiration(Date.from(validityPeriod.atZone(ZoneId.systemDefault()).toInstant()))
                   .signWith(SignatureAlgorithm.HS256, JWT_SECRET)
                   .compact();
    }
    
    public Authentication queryAuthentication(final String token) throws StandardException, SQLException {
        final String username = Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(token).getBody().getSubject();
        UserDetails userDetails = abstractApiClientUserAuthHandler.queryUserViaUsername(username);
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }
    
    public String resolveToken(HttpServletRequest request) {
        String bearerToken = request.getHeader(AUTH_HEADER);
        return bearerToken != null && bearerToken.startsWith(BEARER) ? bearerToken.substring(7) : null;
    }
    
    public boolean isValidToken(final String token) throws StandardException {
        boolean isValidToken = false;
        try {
            Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(token);
            isValidToken = true;
        } catch (JwtException | IllegalArgumentException aE) {
            log.error("Processing JWT token failed {}", aE.getMessage());
            throw new StandardException(StandardStatusEnum.INVALID_INPUT_PARAMETER, new Object[] {"token"});
        }
        return isValidToken;
    }
    
}
