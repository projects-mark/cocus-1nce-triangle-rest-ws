package pt.cocus.nce.rest.ws.model.enums;
/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 */
public enum TriangleClassificationEnum {

    INVALID("invalid_triangle"),
    EQUILATERAL("equilateral_triangle"),
    ISOSCELES("iscoceles_triangle"),
    SCALENE("scalene_triangle");
    
    private String classification;
    
    public String getClassification() {
        return classification;
    }
    
    TriangleClassificationEnum(final String classification) {
        this.classification = classification;
    }
}
