package pt.cocus.nce.rest.ws.service.abstractions;

import org.springframework.security.core.userdetails.UserDetails;
import pt.cocus.nce.rest.ws.core.exceptions.SQLException;
import pt.cocus.nce.rest.ws.core.exceptions.StandardException;

/**
 * @author mark-mngoma
 * @created_at 30 Jan 2022
 */
public interface ApiClientUserAuthServiceAbstraction {
    
    public static final String BEAN_NAME = "service.ApiClientUserAuthServiceAbstraction";

    UserDetails queryUserViaUsername(final String username) throws StandardException, SQLException;

}
