package pt.cocus.nce.rest.ws.model.dto.response;

import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mark-mngoma
 * @created_at 30 Jan 2022
 */

public class AuthenticationResponse extends BaseResponse {

    private static final long serialVersionUID = 3315363633753812920L;
    @JsonProperty(value = "success_message")
    private String successMessage;
    private String token;
    @JsonProperty(value = "token_expiry")
    private LocalDateTime tokenExpiry;
    
    public String getSuccessMessage() {
        return successMessage;
    }
    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public LocalDateTime getTokenExpiry() {
        return tokenExpiry;
    }
    public void setTokenExpiry(LocalDateTime tokenExpiry) {
        this.tokenExpiry = tokenExpiry;
    }
           
}
