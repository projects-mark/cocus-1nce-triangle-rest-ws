package pt.cocus.nce.rest.ws.web.api.v1.resource;

import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.cocus.nce.rest.ws.core.exceptions.SQLException;
import pt.cocus.nce.rest.ws.core.exceptions.StandardException;
import pt.cocus.nce.rest.ws.core.utils.HeaderUtil;
import pt.cocus.nce.rest.ws.core.utils.RestResponseUtil;
import pt.cocus.nce.rest.ws.model.dto.request.TriangleClassificationRequest;
import pt.cocus.nce.rest.ws.model.dto.response.TriangleClassificationQueryResponse;
import pt.cocus.nce.rest.ws.model.dto.response.TriangleClassificationResponse;
import pt.cocus.nce.rest.ws.service.abstractions.TriangleClassificationServiceAbstraction;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 */
@RestController
@RequestMapping(value = "/api/v1/triangle-classification/")
public class TriangleClassificationResource {

    private static final String RESOURCE_REQUEST_MEDIA_TYPE = MediaType.APPLICATION_JSON_VALUE;
    
    private final TriangleClassificationServiceAbstraction abstractTriangleClassificationHandler;

    @Autowired
    public TriangleClassificationResource(@Qualifier(
            value = TriangleClassificationServiceAbstraction.BEAN_NAME) final TriangleClassificationServiceAbstraction abstractTriangleClassificationHandler) {
        this.abstractTriangleClassificationHandler = abstractTriangleClassificationHandler;
    }

    @PostMapping(value = "calc-triangle", consumes = RESOURCE_REQUEST_MEDIA_TYPE,
            produces = RESOURCE_REQUEST_MEDIA_TYPE) 
    public ResponseEntity<TriangleClassificationResponse> processTriangleTypeCalc(@Valid
            @RequestBody final TriangleClassificationRequest request) throws StandardException, SQLException {
        final TriangleClassificationResponse response =
                abstractTriangleClassificationHandler.processTriangleClassificationQuery(request);
        return new ResponseEntity<>(response, response.getStatusCode());
    }

    @GetMapping(value = "query-triangle-histories/{username}", produces = RESOURCE_REQUEST_MEDIA_TYPE)
    public ResponseEntity<List<TriangleClassificationQueryResponse>> queryTriangleHistoriesViaUsername(@PathVariable(required = true, name = "username") final String username) throws StandardException, SQLException {
        final List<TriangleClassificationQueryResponse> response =
                abstractTriangleClassificationHandler.queryTriangleClassificationHistoriesViaUsername(username);
        return new ResponseEntity<>(response, response.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }
    
    @GetMapping(value = "query-triangle-history/{reference_number}", produces = RESOURCE_REQUEST_MEDIA_TYPE)
    public ResponseEntity<TriangleClassificationQueryResponse> queryTriangleHistory(@PathVariable(required = true, name = "reference_number") final String referenceNumber) throws StandardException, SQLException {
        final Optional<TriangleClassificationQueryResponse> response = abstractTriangleClassificationHandler.queryTriangleClassificationHistory(referenceNumber);
        return RestResponseUtil.wrapOrNotFound(response, HeaderUtil.createAlert(this.getClass().getSimpleName(), "SEARCH Request Completed", referenceNumber));
    }
    

    @GetMapping(value = "query-triangle-histories", produces = RESOURCE_REQUEST_MEDIA_TYPE)
    public ResponseEntity<List<TriangleClassificationQueryResponse>> queryTriangleHistories() throws StandardException, SQLException {
        final List<TriangleClassificationQueryResponse> response =
                abstractTriangleClassificationHandler.queryTriangleClassificationHistories();
        return new ResponseEntity<>(response, response.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }
}
