package pt.cocus.nce.rest.ws.config.jwt;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import pt.cocus.nce.rest.ws.core.exceptions.SQLException;
import pt.cocus.nce.rest.ws.core.exceptions.StandardException;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
public class JwtTokenFilter extends OncePerRequestFilter {

    private static final Logger log = LoggerFactory.getLogger(JwtTokenFilter.class);
    
    private JwtTokenProvider jwtTokenProvider;
    
    @Autowired
    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
            FilterChain filterChain) throws ServletException, IOException {
        String token = jwtTokenProvider.resolveToken(request);
        try {
            if (token != null && jwtTokenProvider.isValidToken(token)) {
                Authentication authenticate = jwtTokenProvider.queryAuthentication(token);
                SecurityContextHolder.getContext().setAuthentication(authenticate);
            }
        } catch (StandardException | SQLException e) {
            SecurityContextHolder.clearContext();
            response.sendError(e.getPredefinedStatus().getHttpResponseCode(), e.getMessage());
            log.error("Error finding records of User to be authenticated {} #STATUS_CODE {}", e.getMessage(), e.getPredefinedStatus().getHttpResponseCode());
            return;
        }
        filterChain.doFilter(request, response);
    }
    

}
