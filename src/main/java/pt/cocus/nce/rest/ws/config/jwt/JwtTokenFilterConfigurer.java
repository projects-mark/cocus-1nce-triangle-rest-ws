package pt.cocus.nce.rest.ws.config.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
public class JwtTokenFilterConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity>{

    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    public JwtTokenFilterConfigurer(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }
    
    @Override
    public void configure(HttpSecurity http) {
        JwtTokenFilter customTokenFilter = new JwtTokenFilter(jwtTokenProvider);
        http.addFilterBefore(customTokenFilter,  UsernamePasswordAuthenticationFilter.class);
    }
}
