package pt.cocus.nce.rest.ws.model.enums;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.http.HttpStatus;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 * 
 * Defines the base status enum interface for use with Base exception classes
 *
 * @param <T> Implementation type
 */
public interface BaseStatusEnum<T> {

	BaseStatusEnum<?>[] EMPTY_LIST = new BaseStatusEnum[]{};

    int getErrorCode();

    String getStatusDescription();

    int getCodeRangeMin();

    int getCodeRangeMax();

    int getHttpResponseCode();

    boolean isSuccessStatus();

    default String toStandardString() {
        return ToStringBuilder.reflectionToString(this);
    }

    default String getFormatted(Object... aParameters) {
        return String.format(getStatusDescription(), aParameters);
    }

    T valueOf();

    /**
     * This is meant to block the process when looking for a root cause of an exception
     */
    default boolean isOverrideStatus() {
        return false;
    }

    /**
     * This will allow for specialization by the status enum class since only 203 is considered at this point
     */
    default boolean isRetriable() {
        return this.getHttpResponseCode() == HttpStatus.NON_AUTHORITATIVE_INFORMATION.value();
    }

    static <T extends BaseStatusEnum<?>> boolean isRetryable(T aInstance) {
        return aInstance != null && aInstance.isRetriable();
    }
}
