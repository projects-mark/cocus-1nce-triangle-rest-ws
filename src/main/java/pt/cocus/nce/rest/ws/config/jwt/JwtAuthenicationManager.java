package pt.cocus.nce.rest.ws.config.jwt;

import java.util.ArrayList;
import java.util.List;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pt.cocus.nce.rest.ws.model.enums.Role;

/**
 * @author mark-mngoma
 * @created_at 30 Jan 2022
 */
public class JwtAuthenicationManager implements AuthenticationManager {
    static final List<GrantedAuthority> AUTHORITIES = new ArrayList<>();
  
    static {
      AUTHORITIES.add(new SimpleGrantedAuthority(Role.ROLE_API_CLIENT.name()));
    }
  
    public Authentication authenticate(Authentication auth) {
      final String principalName = auth.getName();
      return new UsernamePasswordAuthenticationToken(principalName, auth.getCredentials(), AUTHORITIES);
    }
}