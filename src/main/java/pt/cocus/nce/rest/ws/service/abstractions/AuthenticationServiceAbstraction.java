package pt.cocus.nce.rest.ws.service.abstractions;

import pt.cocus.nce.rest.ws.core.exceptions.SQLException;
import pt.cocus.nce.rest.ws.core.exceptions.StandardException;
import pt.cocus.nce.rest.ws.model.dto.request.AuthenticationLogonRequest;
import pt.cocus.nce.rest.ws.model.dto.request.AuthenticationRegisterRequest;
import pt.cocus.nce.rest.ws.model.dto.response.AuthenticationResponse;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
public interface AuthenticationServiceAbstraction {
    
    public static final String BEAN_NAME = "service.AuthenticationServiceAbstraction";
    
    AuthenticationResponse processOnboarding(final AuthenticationRegisterRequest registerRequest) throws StandardException, SQLException;
    AuthenticationResponse processSignOn(final AuthenticationLogonRequest logonRequest) throws StandardException, SQLException;

}
