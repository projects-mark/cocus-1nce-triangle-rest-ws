package pt.cocus.nce.rest.ws.model.enums;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author mark-mngoma
 * @created_at 30 Jan 2022
 */
public enum Role implements GrantedAuthority {

    ROLE_API_CLIENT;

    @Override
    public String getAuthority() {
        return name();
    }
    
}
