package pt.cocus.nce.rest.ws.model.dto.response;

import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 */
public class TriangleClassificationQueryResponse extends BaseResponse {

    private static final long serialVersionUID = -8654520754838992477L;
    
    @JsonProperty(value = "triangle_classification_id")
    private long triangleClassificationId;
    @JsonProperty(value = "triangle_reference_number")
    private String triangleReferenceNumber;
    @JsonProperty(value = "triangle_type")
    private String triangleType;
    @JsonProperty(value = "user_id")
    private long userId;
    @JsonProperty(value = "created_at")
    private LocalDateTime createdAt;

    @JsonProperty(value = "updated_at")
    private LocalDateTime updatedAt;
    
    public long getTriangleClassificationId() {
        return triangleClassificationId;
    }
    public void setTriangleClassificationId(long triangleClassificationId) {
        this.triangleClassificationId = triangleClassificationId;
    }
    
    public String getTriangleReferenceNumber() {
        return triangleReferenceNumber;
    }
    public void setTriangleReferenceNumber(String triangleReferenceNumber) {
        this.triangleReferenceNumber = triangleReferenceNumber;
    }
    public String getTriangleType() {
        return triangleType;
    }
    public void setTriangleType(String triangleType) {
        this.triangleType = triangleType;
    }
    public long getUserId() {
        return userId;
    }
    public void setUserId(long userId) {
        this.userId = userId;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }    
        
}
