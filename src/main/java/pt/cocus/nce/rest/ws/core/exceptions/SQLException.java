package pt.cocus.nce.rest.ws.core.exceptions;

import pt.cocus.nce.rest.ws.model.enums.BaseStatusEnum;
import pt.cocus.nce.rest.ws.model.enums.SQLStatusEnum;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 * Exception class used for SQL exceptions
 */
public class SQLException extends BaseException {

	private static final long serialVersionUID = 8809562526985971004L;

	public SQLException(final String message) {
        super(message);
    }

    public SQLException(final Throwable e) {
        super(e);
    }

    public SQLException(final String message, final Throwable e) {
        super(message, e);
    }

    public SQLException(final SQLStatusEnum aPredefinedError) {
        super(aPredefinedError);
    }

    public SQLException(final SQLStatusEnum aPredefinedError, final Throwable e) {
        super(aPredefinedError, e);
    }

    public SQLException(final SQLStatusEnum aPredefinedError, final Throwable e, final String aAdditionalInfo) {
        super(aPredefinedError, e, aAdditionalInfo);
    }

    public SQLException(final SQLStatusEnum aPredefinedError, final String aAdditionalInfo) {
        super(aPredefinedError, aAdditionalInfo);
    }

    public SQLException(final SQLStatusEnum aPredefinedError, final Throwable e, final Object... aSubstitutionValues) {
        super(aPredefinedError, e, aSubstitutionValues);
    }

    public SQLException(final SQLStatusEnum aPredefinedError, final Object... aSubstitutionValues) {
        super(aPredefinedError, aSubstitutionValues);
    }

    public static Class<? extends BaseStatusEnum<?>> getPredefinedStatusType() throws BaseException {
        return SQLStatusEnum.class;
    }
}
