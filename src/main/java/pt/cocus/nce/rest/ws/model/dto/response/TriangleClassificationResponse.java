package pt.cocus.nce.rest.ws.model.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 */
public class TriangleClassificationResponse extends BaseResponse {

    private static final long serialVersionUID = 6005283978600344884L;
    
    @JsonProperty(value = "triangle_type")
    private String triangleType;
    
    @JsonProperty(value = "triangle_reference_number")
    private String triangleReferenceNumber;

    public String getTriangleType() {
        return triangleType;
    }
    public void setTriangleType(String triangleType) {
        this.triangleType = triangleType;
    }
    public String getTriangleReferenceNumber() {
        return triangleReferenceNumber;
    }
    public void setTriangleReferenceNumber(String triangleReferenceNumber) {
        this.triangleReferenceNumber = triangleReferenceNumber;
    }
    
}
