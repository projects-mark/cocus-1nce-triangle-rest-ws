package pt.cocus.nce.rest.ws.config;

import static pt.cocus.nce.rest.ws.config.SpringfoxConfigProperties.BEAN_NAME;
import static pt.cocus.nce.rest.ws.config.SpringfoxConfigProperties.CONFIG_PROEPRTIES_PREFIX;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author mark-mngoma
 * @created_at 29 Jan 2022
 */
@Configuration(value = BEAN_NAME)
@ConfigurationProperties(prefix = CONFIG_PROEPRTIES_PREFIX)
public class SpringfoxConfigProperties {
    
    public static final String BEAN_NAME = "config.SpringfoxConfigProperties";
    public static final String CONFIG_PROEPRTIES_PREFIX = "api-docs";
    
    private String title;
    private String description;
    private String termsOfServiceUrl;
    private String contactName;
    private String contactUrl;
    private String contactEmail;
    private String license;
    private String licenseUrl;
    private String version;
    
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getTermsOfServiceUrl() {
        return termsOfServiceUrl;
    }
    public void setTermsOfServiceUrl(String termsOfServiceUrl) {
        this.termsOfServiceUrl = termsOfServiceUrl;
    }
    public String getContactName() {
        return contactName;
    }
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }
    public String getContactUrl() {
        return contactUrl;
    }
    public void setContactUrl(String contactUrl) {
        this.contactUrl = contactUrl;
    }
    public String getContactEmail() {
        return contactEmail;
    }
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }
    public String getLicense() {
        return license;
    }
    public void setLicense(String license) {
        this.license = license;
    }
    public String getLicenseUrl() {
        return licenseUrl;
    }
    public void setLicenseUrl(String licenseUrl) {
        this.licenseUrl = licenseUrl;
    }
    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }
    
    
}
