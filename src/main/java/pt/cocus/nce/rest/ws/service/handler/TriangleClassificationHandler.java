package pt.cocus.nce.rest.ws.service.handler;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pt.cocus.nce.rest.ws.core.exceptions.SQLException;
import pt.cocus.nce.rest.ws.core.exceptions.StandardException;
import pt.cocus.nce.rest.ws.core.utils.GetterUtil;
import pt.cocus.nce.rest.ws.core.utils.JdbcRequestUtil;
import pt.cocus.nce.rest.ws.core.utils.ObjectMapperUtil;
import pt.cocus.nce.rest.ws.model.dto.request.TriangleClassificationRequest;
import pt.cocus.nce.rest.ws.model.dto.response.TriangleClassificationQueryResponse;
import pt.cocus.nce.rest.ws.model.dto.response.TriangleClassificationResponse;
import pt.cocus.nce.rest.ws.model.enums.TriangleClassificationEnum;
import pt.cocus.nce.rest.ws.service.abstractions.TriangleClassificationServiceAbstraction;
import pt.cocus.nce.triangle.domain.dao.ApiClientUserDao;
import pt.cocus.nce.triangle.domain.dao.TriangleClassificationDao;
import pt.cocus.nce.triangle.domain.dao.query.ApiClientUserQuery;
import pt.cocus.nce.triangle.domain.dao.query.TriangleClassificationQuery;
import pt.cocus.nce.triangle.domain.entity.ApiClientUserEntity;
import pt.cocus.nce.triangle.domain.entity.TriangleClassificationEntity;

/**
 * @author mark-mngoma
 * @created_at 28 Jan 2022
 */
@Service(value = TriangleClassificationServiceAbstraction.BEAN_NAME)
public class TriangleClassificationHandler implements TriangleClassificationServiceAbstraction {

    private static final int ZERO_INT_VALUE = 0;

    private final TriangleClassificationDao triangleClassificationDao;
    private final ApiClientUserDao apiClientUserDao;

    @Autowired
    public TriangleClassificationHandler(@Qualifier(value = TriangleClassificationDao.BEAN_NAME) final TriangleClassificationDao triangleClassificationDao, 
                                         @Qualifier(value = ApiClientUserDao.BEAN_NAME) final ApiClientUserDao apiClientUserDao) {
        this.triangleClassificationDao = triangleClassificationDao;
        this.apiClientUserDao = apiClientUserDao;
    }

    @Override
    public TriangleClassificationResponse processTriangleClassificationQuery(final TriangleClassificationRequest request) throws StandardException, SQLException {
        TriangleClassificationResponse response = new TriangleClassificationResponse();
        
        final int pointX = request.getPointX();
        final int pointY = request.getPointY();
        final int pointZ = request.getPointZ();
        
        boolean isValidParameters = pointX + pointY < pointZ || pointX + pointZ < pointY || pointY + pointZ > pointX;
        boolean isNotContainsZeroValue = (pointX != ZERO_INT_VALUE && pointY != ZERO_INT_VALUE && pointZ != ZERO_INT_VALUE);
        boolean isValidRequest = isValidParameters && isNotContainsZeroValue;
        
        if (isValidRequest) {
            if (pointX == pointY && pointY == pointZ) {
                response.setTriangleType(TriangleClassificationEnum.EQUILATERAL.getClassification());
            } else if (pointX == pointY || pointY == pointZ || pointZ == pointX) {
                response.setTriangleType(TriangleClassificationEnum.ISOSCELES.getClassification());
            } else {
                response.setTriangleType(TriangleClassificationEnum.SCALENE.getClassification());
            }
        } else {
            response.setTriangleType(TriangleClassificationEnum.INVALID.getClassification());
        }
        
        final TriangleClassificationEntity entityRequest = constructTriangleEntity(response);  
        JdbcRequestUtil.insert(triangleClassificationDao::insertTriangleClassification, "TriangleClassificationDao::insertTriangleClassification", entityRequest);
        response.setTriangleReferenceNumber(GetterUtil.safeGet(entityRequest, TriangleClassificationEntity::getTriangleReferenceNumber));
        response.setValidRequest(isValidRequest);
        response.setStatusCode(response.isValidRequest() ? HttpStatus.CREATED : HttpStatus.UNPROCESSABLE_ENTITY);
        return response;
    }
    
    @Override
    public List<TriangleClassificationQueryResponse> queryTriangleClassificationHistories() throws StandardException, SQLException {
        List<TriangleClassificationEntity> entityQueryResults =
                JdbcRequestUtil.select(triangleClassificationDao::queryTriangleClassificationHistories, "triangleClassificationDao::queryTriangleClassificationHistories");
        return ObjectMapperUtil.mapAll(entityQueryResults, TriangleClassificationQueryResponse.class);
    }

    @Override
    public List<TriangleClassificationQueryResponse> queryTriangleClassificationHistoriesViaUsername(final String username) throws StandardException, SQLException {
        List<TriangleClassificationEntity> entityQueryResult = 
        JdbcRequestUtil.select(triangleClassificationDao::queryTriangleClassificationHistoriesViaLogin, "triangleClassificationDao::queryTriangleClassificationHistoriesViaLogin", 
                constructTriangleTypeQuery(username, null));
        return ObjectMapperUtil.mapAll(entityQueryResult, TriangleClassificationQueryResponse.class);
    }
    
    @Override
    public Optional<TriangleClassificationQueryResponse> queryTriangleClassificationHistory(
           final String referenceNumber) throws StandardException, SQLException {
        Optional<TriangleClassificationEntity> response = JdbcRequestUtil.select(triangleClassificationDao::queryTriangleClassificationHistory, "triangleClassificationDao::queryTriangleClassificationHistory", 
                constructTriangleTypeQuery(null, referenceNumber));
        if (response.isPresent()) {
            return Optional.of(ObjectMapperUtil.map(response.get(), TriangleClassificationQueryResponse.class));
        }
        
        TriangleClassificationQueryResponse emptyResponse =  new TriangleClassificationQueryResponse();
        emptyResponse.setValidRequest(false);
        emptyResponse.setStatusCode(HttpStatus.NOT_FOUND);
        return Optional.of(emptyResponse);
    }
    
    private long getCurrentUser() throws StandardException, SQLException {
      long currentUserId = 0l;
      ApiClientUserQuery query = new ApiClientUserQuery();
      query.setApiClientUserLogin(SecurityContextHolder.getContext().getAuthentication().getName());
      final Optional<ApiClientUserEntity> queryResult = JdbcRequestUtil.select(apiClientUserDao::queryClientByUsername, "apiClientUserDao::queryClientByUsername", query);
      if (queryResult.isPresent()) {
        currentUserId = GetterUtil.coalesce(queryResult.get().getUserId(), currentUserId);
      }
      return currentUserId;
    }
    
    
    private TriangleClassificationQuery constructTriangleTypeQuery(final String username, final String referenceNumber) {
        TriangleClassificationQuery query = new TriangleClassificationQuery();
        if (referenceNumber == null) {
            query.setUsername(username);
        } else {   
            query.setTriangleReferenceNumber(referenceNumber);
        }
        return query;
    }
    
    private TriangleClassificationEntity constructTriangleEntity(final TriangleClassificationResponse response) throws StandardException, SQLException {
        TriangleClassificationEntity entity = new TriangleClassificationEntity();
        if (TriangleClassificationEnum.ISOSCELES.getClassification().equals(response.getTriangleType())) {
          entity.setTriangleType(TriangleClassificationEnum.ISOSCELES.name());
        } else if (TriangleClassificationEnum.EQUILATERAL.getClassification().equals(response.getTriangleType())) {
            entity.setTriangleType(TriangleClassificationEnum.EQUILATERAL.name());
        } else if (TriangleClassificationEnum.SCALENE.getClassification().equals(response.getTriangleType())) {
            entity.setTriangleType(TriangleClassificationEnum.SCALENE.name());
        } else {
            entity.setTriangleType(TriangleClassificationEnum.INVALID.name());
        }
        entity.setUserId(getCurrentUser());
        entity.setTriangleReferenceNumber(UUID.randomUUID().toString().toUpperCase());
        entity.setCreatedBy(entity.getUserId());
        return entity;
    }
}
