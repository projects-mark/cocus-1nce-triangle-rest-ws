package pt.cocus.nce.rest.ws;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import pt.cocus.nce.rest.ws.config.jwt.JwtTokenProvider;
import pt.cocus.nce.rest.ws.model.dto.request.AuthenticationLogonRequest;
import pt.cocus.nce.rest.ws.model.dto.request.AuthenticationRegisterRequest;
import pt.cocus.nce.rest.ws.model.dto.request.TriangleClassificationRequest;
import pt.cocus.nce.rest.ws.model.dto.response.AuthenticationResponse;
import pt.cocus.nce.rest.ws.model.dto.response.TriangleClassificationResponse;
import pt.cocus.nce.rest.ws.model.enums.TriangleClassificationEnum;
import pt.cocus.nce.rest.ws.web.api.v1.resource.AuthenticationResource;
import pt.cocus.nce.rest.ws.web.api.v1.resource.TriangleClassificationResource;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(value = { SpringExtension.class, RestDocumentationExtension.class })
@ActiveProfiles("test")
@AutoConfigureRestDocs(outputDir = "/static/docs")
class Cocus1nceTriangleRestWsApplicationTests {

    @Autowired
    MockMvc mockMvc;
    
    @Autowired
    JwtTokenProvider authTokenProvider;
    
    @Autowired
    AuthenticationResource authenticationResource;
    
    @Autowired 
    TriangleClassificationResource triangleClassificationResource;
        
    @Autowired
    ObjectMapper mapper;
    
    @BeforeEach
    public void setup(WebApplicationContext context, RestDocumentationContextProvider restDocumentationContextProvider) {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(documentationConfiguration(restDocumentationContextProvider)).build(); 
    }
    
	@Test
	void contextLoads() {
	    assertThat(authenticationResource).isNotNull();
	    assertThat(triangleClassificationResource).isNotNull();
	    mapper = new ObjectMapper();
	}

	@Test
	void whenClientIsUnauthorizedWithoutBearerToken_revert() throws Exception {
	    this.mockMvc.perform(get("/api/v1/triangle-classification/query-triangle-histories")).andDo(print()).andDo(document("api/v1/triangle-classification/query-triangle-histories")).andExpect(status().isOk());
	}
	
	@Test
	void whenClientOnboards_revert201() throws JsonProcessingException, Exception {
	    AuthenticationRegisterRequest request = new AuthenticationRegisterRequest();
	    request.setUsername(generateUniqueEmail());
	    request.setPassword(UUID.randomUUID().toString().toLowerCase().replace("-", ""));
	    MvcResult result = mockMvc.perform(
	            RestDocumentationRequestBuilders.
	            post("/api/v1/authentication/on-board")
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(mapper.writeValueAsString(request))
	            .accept(MediaType.APPLICATION_JSON))
	            .andDo(print()).andDo(document("api/v1/authentication/on-board"))
	            .andExpect(status().isCreated())
	            .andReturn();
	    final AuthenticationResponse signupResponse = 
                mapper.readValue(result.getResponse().getContentAsString(), AuthenticationResponse.class);
	    assertNotNull(signupResponse);
	    assertEquals(HttpStatus.CREATED, signupResponse.getStatusCode());
	    assertEquals(null, signupResponse.getToken());
	    assertTrue(signupResponse.isValidRequest());
	}
	
	@Test
	void whenClientHasValidCredentials_revertResponseJwtTokenPerformOperations() throws Exception {
	    final String email = generateUniqueEmail();
	    final String password = UUID.randomUUID().toString().toLowerCase().replace("-", "");
	    AuthenticationRegisterRequest request = new AuthenticationRegisterRequest();
        request.setUsername(email);
        request.setPassword(password);
        
        mockMvc.perform(post("/api/v1/authentication/on-board")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print()).andDo(document("api/v1/authentication/on-board"))
                .andExpect(status().isCreated())
                .andReturn();
	    
	    AuthenticationLogonRequest logonRequest = new AuthenticationLogonRequest();
	    logonRequest.setUsername(email);
	    logonRequest.setPassword(password);
	    
	    MvcResult logonResult = mockMvc.perform(post("/api/v1/authentication/sign-on")
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(mapper.writeValueAsString(logonRequest))
	            .accept(MediaType.APPLICATION_JSON))
	            .andDo(print()).andDo(document("api/v1/authentication/sign-on"))
	            .andExpect(status().isOk())
	            .andReturn();
	    
	    JsonNode parent = mapper.readTree(logonResult.getResponse().getContentAsString());
	    String authToken = parent.get("token").asText();
	    assertThat(authToken).isNotNull();
	    boolean isValidJwtToken = authTokenProvider.isValidToken(authToken);
	    
	    // POST to: /api/v1/triangle-classification/calc-triangle
	    final TriangleClassificationRequest trianglePostRequestBody = constructValidRequest();
	    
	    final HttpHeaders httpHeaders = constructValidHttpHeaders(authToken);
	    
	    MvcResult resultResponse = mockMvc.perform(post("/api/v1/triangle-classification/calc-triangle")
	            .headers(httpHeaders)
	            .contentType(MediaType.APPLICATION_JSON)
	            .content(mapper.writeValueAsString(trianglePostRequestBody))
	            .accept(MediaType.APPLICATION_JSON))
	            .andDo(print()).andDo(document("api/v1/triangle-classification/calc-triangle"))
	            .andExpect(status().isCreated())
	            .andReturn();
	    
	    final TriangleClassificationResponse createTriangleResponse = 
	            mapper.readValue(resultResponse.getResponse().getContentAsString(), TriangleClassificationResponse.class);
	    createTriangleResponse.setValidRequest(true);
	    
        mockMvc.perform(get("/api/v1/triangle-classification/query-triangle-history/" + createTriangleResponse.getTriangleReferenceNumber())
                .headers(httpHeaders))
                .andDo(print()).andDo(document("api/v1/triangle-classification/query-triangle-history/{reference_number}"))
                .andExpect(status().isOk())
                .andReturn();

        // Assertions
        assertTrue(isValidJwtToken);
        assertEquals(createTriangleResponse.getTriangleType(), TriangleClassificationEnum.EQUILATERAL.getClassification());
        assertTrue(createTriangleResponse.isValidRequest());
	}
	
	private TriangleClassificationRequest constructValidRequest() {
	    TriangleClassificationRequest request = new TriangleClassificationRequest();
	    request.setPointX(5);
	    request.setPointY(5);
	    request.setPointZ(5);
	    return request;
	}
	
	private HttpHeaders constructValidHttpHeaders(final String token) {
	    HttpHeaders headers = new HttpHeaders();
	    headers.add("Authorization", "Bearer " + token);
	    return headers;
	}
	
	private static String getUniqueId() {
        return String.format("%s_%s", UUID.randomUUID().toString().substring(0, 5), System.currentTimeMillis() / 1000);
    }
	
	public static String generateUniqueEmail() {
        return String.format("%s@%s", getUniqueId(), "cocus.pt");
    }
}
