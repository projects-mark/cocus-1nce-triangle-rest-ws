package pt.cocus.nce.rest.ws.service.handler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import pt.cocus.nce.rest.ws.core.exceptions.SQLException;
import pt.cocus.nce.rest.ws.core.exceptions.StandardException;
import pt.cocus.nce.rest.ws.model.dto.request.TriangleClassificationRequest;
import pt.cocus.nce.rest.ws.model.dto.response.TriangleClassificationResponse;
import pt.cocus.nce.rest.ws.model.enums.TriangleClassificationEnum;
import pt.cocus.nce.triangle.domain.dao.ApiClientUserDao;
import pt.cocus.nce.triangle.domain.dao.TriangleClassificationDao;

/**
 * @author mark-mngoma
 * @created_at 30 Jan 2022
 */
@ExtendWith(value = MockitoExtension.class)
class TriangleClassificationHandlerTest {
    
    
    @InjectMocks
    TriangleClassificationHandler handler;
    
    @Mock
    TriangleClassificationDao triangleClassificationDao;
    
    @Mock
    ApiClientUserDao apiClientUserDao;
    
    @BeforeEach
    void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    void testEquilateralTriangleClassification() throws StandardException, SQLException {
        final TriangleClassificationResponse response = handler.processTriangleClassificationQuery(globalRequest());
        assertNotNull(response);
        assertNotNull(response.getTriangleReferenceNumber());
        assertEquals(response.getTriangleType(), TriangleClassificationEnum.EQUILATERAL.getClassification());
    }
    
    @Test
    void testIsoscelesTriangleTriangleClassifaction() throws StandardException, SQLException {
        TriangleClassificationRequest isoscelesRequest = globalRequest();
        isoscelesRequest.setPointZ(3);
        final TriangleClassificationResponse response = handler.processTriangleClassificationQuery(isoscelesRequest);
        assertNotNull(response);
        assertNotNull(response.getTriangleReferenceNumber());
        assertEquals(response.getTriangleType(), TriangleClassificationEnum.ISOSCELES.getClassification());
    }
    
    @Test
    void testScaleneTriangleClassification() throws StandardException, SQLException {
        TriangleClassificationRequest scaleneRequest = globalRequest();
        scaleneRequest.setPointX(51);
        scaleneRequest.setPointY(92);
        final TriangleClassificationResponse response = handler.processTriangleClassificationQuery(scaleneRequest);
        assertNotNull(response);
        assertNotNull(response.getTriangleReferenceNumber());
        assertEquals(response.getTriangleType(), TriangleClassificationEnum.SCALENE.getClassification());
    }
    
    @Test
    void testInvalidTriangleClassification() throws StandardException, SQLException {
        TriangleClassificationRequest emptyRequest = new TriangleClassificationRequest();
        emptyRequest.setPointX(0);
        emptyRequest.setPointY(0);
        emptyRequest.setPointZ(0);
        final TriangleClassificationResponse response = handler.processTriangleClassificationQuery(emptyRequest);
        assertNotNull(response);
        assertNotNull(response.getTriangleReferenceNumber());
        assertEquals(response.getTriangleType(), TriangleClassificationEnum.INVALID.getClassification());
    }
    
    private TriangleClassificationRequest globalRequest() {
        TriangleClassificationRequest request = new TriangleClassificationRequest();
        request.setPointX(17);
        request.setPointY(17);
        request.setPointZ(17);
        return request;
    }
}
