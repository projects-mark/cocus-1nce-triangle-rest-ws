# Cocus 1NCE Triangle Classification REST API Springboot WebService
The application uses the current Springboot version (v2.6.0), do refer to the project requirement for the scope of work required:

* It is composed of the domain layer package using MyBatis: `pt.cocus.nce.triangle.domain.*`
* MyBatis is a persistence framework, not an ORM, allowing you to map your queries with ease to your POJOs.
* It is useful for highly data driven applications.
* Some context: ORMs come with some bootlenecks and rather write your own queries and map the results yourself to get the best performance out of the RDBMS.
* For migrations we're making full use of DDL based database migration library `Flyway Teams (Enterprise Edition)` to make updates to our domain.
* The domain layer depends on a BOM (Bill of Materials) pom with all domain dependecies, hosted on AWS based JFrog artifactory: 
[JFrog Artifactory Cocus-1nce Libraries](https://assertion.jfrog.io/artifactory/cocus-1nce-libs-all)
* [The domain BOM pom](https://assertion.jfrog.io/artifactory/cocus-1nce-libs-all/pt/cocus/1nce/cocus-1nce-domain-bom/0.0.1-RELEASE/cocus-1nce-domain-bom-0.0.1-RELEASE.pom)
* [The domain layer for the REST Service](https://assertion.jfrog.io/artifactory/cocus-1nce-libs-all/pt/cocus/1nce/cocus-1nce-triangle-domain/0.1.1-RELEASE/cocus-1nce-triangle-domain-0.1.1-RELEASE.jar)
* [Domain Library](https://bitbucket.org/projects-mark/cocus-1nce-triangle-domain/src/master/src/main/)
* [Domain Bom](https://bitbucket.org/projects-mark/cocus-1nce-domain-bom/src/master/)
* Then the rest webservice package: `pt.cocus.nce.rest.ws.*`
* Refer to the swagger docs: `${base-address}/cocus-1nce-triangle-rest-ws/swagger-ui.html`


## It is composed of 7 endpoints: 
#### Refer to postman collection [https://bitbucket.org/projects-mark/cocus-1nce-triangle-rest-ws/src/master/cocus-1nce-traingle-rest-ws.postman_collection.json]() and API documentation [https://bitbucket.org/projects-mark/cocus-1nce-triangle-rest-ws/src/master/API-DOCS.adoc](), swagger-ui is `/cocus-1nce-triangle-rest-ws/swagger-ui.html`
#### Authentication - authentication-resource
- `/api/v1/authentication/on-board` - Sign-Up User
- `/api/v1/authentication/sign-on` - Logon with existing user details, verify JWT token and user retrieves it in response.

#### Triangle Classification
- This requires the bearer token to be passed in order to make requests to the server via the Authorization header: `Bearer {token}`
- `/api/v1/triangle-classification/calc-triangle` - Save calc results of triangle type and give a response.
- `/api/v1/triangle-classification/query-triangle-histories` - Track all histories. `TODO`: add pagination and DB based user roles, only admin should have access this this endpoint.
- `/api/v1/triangle-classification/query-triangle-histories/{username}` -
Track histories via your username `api-user@cocus-api.com`.
- `/api/v1/triangle-classification/query-triangle-history/{reference_number}` - Track a single history for purposes required by the API user, maybe to track on their side and be able to compile reports on each request.


# Getting Started

### Loading the project for development
You'd need Java 8 and Docker, alongside docker-compose to get started with the project, here are some steps to get things rolling:

* `git clone https://markmng@bitbucket.org/projects-mark/cocus-1nce-triangle-rest-ws.git`
* Within your IDE, select `Import` -> `Existing Maven Projects` (Eclipse for me, yours should be the similar)
*  Make sure that the project is able to pull all maven imports and dependecies;
* Set your environment variables for the Flyway Enterprise Migration License Key: `export license_key={license_key_value}`
* Verify that it has been added/set type `echo $license_key` within `/etc/environment`
* NOTE* `If this is not done`, the application will fail to execute for profiles other than `production`
* On Windows it should be System Variables under settings.
* Else this could done in the IDE itself, via `Run Configurations` -> Add the run configurations for the tests project and the springboot ws in Environment, set the key and the value.
* To get your Database lined up, run `docker-compose -f ./cocus-1nce-triangle-server-inf/cocus-1nce-db.yml up`
* Read the docker compose file: `/cocus-1nce-triangle-server-inf/cocus-1nce-db.yml`, get DB logins and verify till you're satisfied.

### Running without debugging on CLI:
* Set your spring profile via the argument, using -D...
* Via the maven wrapper: `./mvnw spring-boot:run -Dspring-boot.run.profiles=localhost`
* Via your maven installation: `mvn spring-boot:run -Dspring-boot.run.profiles=localhost`
* Running via the compiled .jar file `java -jar -Dspring.profiles.active=localhost ./target/cocus-1nce-triangle-rest-ws.jar`

### Boostraping our .jar for it's final destination.
* This would be usually executed by an automated build process on a build server like `Jenkins`, however, that'd be overkill for a demo.
* To get the server ready for some `AWS ECS` ride, run the build : `mvn test` for sanity checks and `mvn clean package` to compile your jar

### Getting Cloudformation ready
* Our CDK process will pick-up the docker base artifact for building and publishing the image from `Dockerfile` to `ECR`, you could also use `JFrog Artifactory` for this, preferred `AWS ECR` in this case.
* Do make sure that you refer to the `AWS CDK Cloudformation Typescript Project`.
* CDK Repository Location: [Typescript CDK](https://bitbucket.org/projects-mark/cocus-1nce-triangle-server-inf/src/master/)
* Clone this repo in the same directory as the Springboot REST API work directory, e.g. `~/jee-workspace/cocus-projects/`
* Follow instructions: [Starting Up Project](https://bitbucket.org/projects-mark/cocus-1nce-triangle-server-inf/src/master/README.md)
* Do make sure that you have configured `AWS` settings in `./cocus-1nce-triangle-server-inf/bin/cocus-1nce-triangle-server-inf.ts`
* You should see something like this, be aware of the environment variables and ensure that you have set everything correctly on your machine: `env: { account: process.env.AWS_ACCOUNT, region: 'eu-west-1' }`
* Alternatively install aws-cli on your machine `sudo apt install awscli` alternatively [AWS Cli Guide](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
* Run command: `aws configure`, you'll be prompted to enter some credentials, do refer to the documentation for more info.
* After deployment refer to `Cloudwatch` logs to your satisfaction.
* If nothing is breaking, do start making a few api calls to the web address provided after successful CDK build process.

### What I have not covered (out of scope)
* Rate limiting via AWS as per the optional requirement.
* Coverage and implementation of `AWS API Gateway`
* Automated load testing via JMeter API
### Guides
The following guides illustrate how to use some features concretely, or extend the project need be:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.6.3/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.6.3/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.6.3/reference/htmlsingle/#boot-features-developing-web-applications)
* [MyBatis Official Documentation](https://mybatis.org/mybatis-3/getting-started.html)
* [AWS CDK Documentation with developer guide](https://docs.aws.amazon.com/cdk/api/v2/)
* [AWS Cloudwatch](https://aws.amazon.com/cloudwatch/)
* [AWS Fargate](https://aws.amazon.com/fargate/)
* [AWS ECS](https://aws.amazon.com/ecs/)
* [AWS Aurora](https://aws.amazon.com/rds/aurora/serverless/)