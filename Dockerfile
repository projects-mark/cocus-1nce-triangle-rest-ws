FROM openjdk:8
ADD target/cocus-1nce-triangle-rest-ws.jar cocus-1nce-triangle-rest-ws.jar
ENTRYPOINT ["java", "-Dspring.profiles.active=production", "-jar", "cocus-1nce-triangle-rest-ws.jar"]
EXPOSE 19041